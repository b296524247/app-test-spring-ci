package uz.pdp.apptestspringci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppTestSpringCiApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppTestSpringCiApplication.class, args);
    }

}
